package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils


sealed interface ResponseStatus {
    object LOADING : ResponseStatus
    class SUCCESS<T>(val response: T, isLocal: Boolean = false) : ResponseStatus
    class ERROR(val error: Throwable) : ResponseStatus
}