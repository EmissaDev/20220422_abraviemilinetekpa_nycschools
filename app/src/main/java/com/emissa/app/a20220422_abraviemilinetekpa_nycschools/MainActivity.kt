package com.emissa.app.a20220422_abraviemilinetekpa_nycschools

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupActionBarWithNavController
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.R
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint


/**
 * Main activity of the app, holds all the fragment that present the school data to the user
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding by lazy {
        ActivityMainBinding.inflate(layoutInflater)
    }
    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        val navHostFragment = supportFragmentManager.findFragmentById(
            R.id.main_nav_host
        ) as NavHostFragment
        navController = navHostFragment.navController

        setupActionBarWithNavController(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }
}