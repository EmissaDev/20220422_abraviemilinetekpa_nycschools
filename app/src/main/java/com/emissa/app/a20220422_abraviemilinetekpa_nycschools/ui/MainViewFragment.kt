package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.R
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.databinding.FragmentMainViewBinding
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.BaseFragment


/**
 * A simple [Fragment] subclass.
 *
 * Welcome screen to instruct the user on what the app does and how the can use it
 * More instructions will be provided later
 */
class MainViewFragment : BaseFragment() {

    private val binding by lazy {
        FragmentMainViewBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        // navigates to the screen that displays the list of schools
        binding.btnContinue.setOnClickListener {
            findNavController().navigate(R.id.action_mainView_to_schoolsList)
        }

        // Inflate the layout for this fragment
        return binding.root
    }
}