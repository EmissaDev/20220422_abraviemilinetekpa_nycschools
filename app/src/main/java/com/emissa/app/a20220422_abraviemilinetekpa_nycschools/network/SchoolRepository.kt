package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.network

import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.FailedResponseException
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.NullResponseException
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.ResponseStatus
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


interface SchoolRepository {
    fun getAllSchools() : Flow<ResponseStatus>
    fun getSatScores(schoolDbn: String) : Flow<ResponseStatus>
}


class SchoolRepositoryImpl @Inject constructor(
    private val apiService: ApiService
) : SchoolRepository {
    override fun getAllSchools(): Flow<ResponseStatus> =
        flow {
            emit(ResponseStatus.LOADING)

            try {
                val response = apiService.getAllSchools()
                if (response.isSuccessful) {
                    response.body()?.let {
                        emit(ResponseStatus.SUCCESS(it))
                    } ?: throw NullResponseException()
                }
                else throw FailedResponseException()
            } catch (e: Exception) {
                emit(ResponseStatus.ERROR(e))
            }
        }

    override fun getSatScores(schoolDbn: String): Flow<ResponseStatus> =
        flow {
            emit(ResponseStatus.LOADING)

            try {
                val response = apiService.getSATScores(schoolDbn)
                if (response.isSuccessful) {
                    response.body()?.let {
                        emit(ResponseStatus.SUCCESS(it))
                    } ?: throw NullResponseException()
                }
                else throw FailedResponseException()
            } catch (e: Exception) {
                emit(ResponseStatus.ERROR(e))
            }
        }
}