package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.School
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.network.SchoolRepository
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.BaseViewModel
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.ResponseStatus
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * View model to observe the scool live data and the score to retireve
 *
 * If time allowed, I would have save data locally to persist it in order to allow user to see the list when offline
 */
@HiltViewModel
class SchoolsViewModel @Inject constructor(
//    private val databaseRepo: DatabaseRepository,
    private val schoolRepo: SchoolRepository,
    private val ioDispatcher: CoroutineDispatcher
) : BaseViewModel() {

    // live data to retrieve the list of school
    private val _schools: MutableLiveData<ResponseStatus> = MutableLiveData()
    val schools: LiveData<ResponseStatus> get() = _schools

    // live data to retrieve the selected school's SAT scores
    private val _scores: MutableLiveData<ResponseStatus> = MutableLiveData()
    val satScore: LiveData<ResponseStatus> get() = _scores

    var school : School? = null


    // get the school list from the server
    fun getAllSchools() {
        viewModelSafeScope.launch(ioDispatcher) {
            schoolRepo.getAllSchools().collect() {
                _schools.postValue(it)
            }
        }
    }

    // get the SAT scores corresponding to the selected school from the list
    fun getSchoolScores(schoolDbn: String) {
        viewModelSafeScope.launch {
            schoolRepo.getSatScores(schoolDbn).collect() {
                _scores.postValue(it)
            }
        }
    }

    // Resets the score data once the user returns to the list of school from the details view
    fun resetScore() {
        _scores.value = ResponseStatus.LOADING
    }
}