package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils



class NullResponseException(
    message: String = "Response is null"
) : Exception(message)

class FailedResponseException(
    message: String = "Error: failure in the response"
) : Exception(message)
