package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data

import androidx.room.*
import androidx.room.OnConflictStrategy.REPLACE
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.School
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.Scores
import kotlinx.coroutines.flow.Flow


@Database(
    entities = [School::class, Scores::class],
    version = 1,
    exportSchema = false
)
abstract class SchoolsDatabase : RoomDatabase() {
    abstract fun getDao(): SchoolsDao
}



@Dao
interface SchoolsDao {
    @Insert(onConflict = REPLACE)
    suspend fun insertSchool(school: School)

    @Insert(onConflict = REPLACE)
    suspend fun insertSchools(schools: List<School>)

    @Query("SELECT * from school")
    fun getAllSchools() : Flow<List<School>>

    @Query("SELECT * from school WHERE dbn = :dbn")
    fun getSchool(dbn: String) : Flow<School>

    @Query("SELECT * from scores WHERE dbn = :dbn")
    fun getSchoolScore(dbn: String): Flow<Scores>

    @Delete
    suspend fun deleteSchool(school: School)

    @Delete
    suspend fun deleteSchools(schools: List<School>)
}