package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.di

import android.content.Context
import androidx.room.Room
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data.DatabaseRepository
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data.DatabaseRepositoryImpl
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data.SchoolsDao
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data.SchoolsDatabase
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.network.ApiService
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.network.SchoolRepository
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.network.SchoolRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton


/**
 * Providing module to instruct Hilt on how to provide the needed instances
 */
@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {

    @Provides
    @Singleton
    fun providesOkHttpClient(): OkHttpClient =
        OkHttpClient.Builder()
            .addInterceptor(HttpLoggingInterceptor().apply {
                level = HttpLoggingInterceptor.Level.BODY
            })
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .build()

    @Provides
    @Singleton
    fun providesNetworkService(okHttpClient: OkHttpClient): ApiService =
        Retrofit.Builder()
            .baseUrl(ApiService.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()
            .create(ApiService::class.java)

    @Provides
    @Singleton
    fun providesSchoolRepository(apiService: ApiService): SchoolRepository =
        SchoolRepositoryImpl(apiService)

    @Provides
    @Singleton
    fun providingDatabase(@ApplicationContext context: Context): SchoolsDatabase {
        return Room.databaseBuilder(
            context,
            SchoolsDatabase::class.java,
            "hs_database"
        )
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    fun providingSchoolDao(database: SchoolsDatabase): SchoolsDao = database.getDao()

    @Singleton
    @Provides
    fun providingMainRepository(dao: SchoolsDao): DatabaseRepository = DatabaseRepositoryImpl(dao)


    @Singleton
    @Provides
    fun providesIODispatcher(): CoroutineDispatcher = Dispatchers.IO

}