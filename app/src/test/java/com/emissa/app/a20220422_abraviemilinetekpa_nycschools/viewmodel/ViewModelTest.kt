package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.School
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.network.SchoolRepository
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.ResponseStatus
import com.google.common.truth.Truth.assertThat
import io.mockk.clearAllMocks
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestScope
import kotlinx.coroutines.test.UnconfinedTestDispatcher
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.lang.Exception


@ExperimentalCoroutinesApi
class ViewModelTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    private val testDispatcher = UnconfinedTestDispatcher()
    private val testScope = TestScope(testDispatcher)
    private val mockRepository = mockk<SchoolRepository>(relaxed = true)
    private lateinit var testObject: SchoolsViewModel

    @Before
    fun setUpTest() {
        Dispatchers.setMain(testDispatcher)
        testObject = SchoolsViewModel(mockRepository, testDispatcher)
    }


    @Test
    fun `get schools list when fetching data from server returns loading response`() {
        every {
            mockRepository.getAllSchools()
        } returns flowOf(ResponseStatus.LOADING)
        val responseListHelper = mutableListOf<ResponseStatus>()
        testObject.schools.observeForever {
            responseListHelper.add(it)
        }
        testObject.getAllSchools()
        assertThat(responseListHelper[1]).isInstanceOf(ResponseStatus.LOADING::class.java)
    }

    @Test
    fun `get schools list when fetching data from server returns successful response`() {
        every {
            mockRepository.getAllSchools()
        } returns flowOf(ResponseStatus.SUCCESS(listOf(mockk<School>()), false))

        val responseListHelper = mutableListOf<ResponseStatus>()
        testObject.schools.observeForever {
            responseListHelper.add(it)
        }
        testObject.getAllSchools()

        val result =  responseListHelper[1] as ResponseStatus.SUCCESS<*>
//        assertEquals("id", result.)
    }

    @Test
    fun `get schools list when fetching data from server returns error response`() {
        every {
            mockRepository.getAllSchools()
        } returns flowOf(ResponseStatus.ERROR(Exception("Error fetching data from server")))

        val responseListHelper = mutableListOf<ResponseStatus>()
        testObject.schools.observeForever {
            responseListHelper.add(it)
        }
        testObject.getAllSchools()
        assertThat(responseListHelper[1]).isInstanceOf(ResponseStatus.ERROR::class.java)
    }

    @After
    fun shutdownTest() {
        clearAllMocks()
        Dispatchers.resetMain()
    }
}