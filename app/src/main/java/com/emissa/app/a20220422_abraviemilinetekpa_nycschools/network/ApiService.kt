package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.network

import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.School
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.Scores
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {
    @GET(SCHOOL_PATH)
    suspend fun getAllSchools() : Response<List<School>>

    @GET(SAT_PATH)
    suspend fun getSATScores(
        @Query("dbn") schoolDbn: String
    ) : Response<List<Scores>>

    companion object {
        const val BASE_URL = "https://data.cityofnewyork.us/"
        private const val SCHOOL_PATH = "resource/s3k6-pzi2.json"
        private const val SAT_PATH = "resource/f9bf-2cp4.json"
//        const val APP_TOKEN = "uayDTc7vtrH1lE3SK6wF9MOpN"
    }
}