package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.R
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data.ItemClickedListener
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data.SchoolAdapter
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.databinding.FragmentSchoolsListBinding
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.School
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.BaseFragment
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.ResponseStatus


/**
 * A simple [Fragment] subclass.
 *
 * This shows the list of all NYC High Schools
 * This list is implemented using a recyclerview inside which each school represents a card view
 */
class SchoolsListFragment : BaseFragment(), ItemClickedListener {

    private val binding by lazy {
        FragmentSchoolsListBinding.inflate(layoutInflater)
    }
    private val mAdapter by lazy {
        SchoolAdapter(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        // Apply the linear vertical oriented layout manager to the recycler view
        // And attach its adapter
        binding.schoolsRecycler.apply {
            layoutManager = LinearLayoutManager(
                requireContext(),
                LinearLayoutManager.VERTICAL,
                false
            )
            adapter = mAdapter
        }

        // Observe the list of school data and populate the view based on the network response
        // If time allowed, will populate local data when device is offline
        schoolsViewModel.schools.observe(viewLifecycleOwner) { state ->
            when(state) {
                is ResponseStatus.LOADING -> {
                    showToastMessage("LOADING...")
                    binding.progressBar.visibility = View.VISIBLE
                    binding.schoolsRecycler.visibility = View.GONE
                    binding.swipeRefresh.visibility = View.GONE
                }
                is ResponseStatus.SUCCESS<*> -> {
                    val retrievedSongs = state.response as List<School>
                    binding.progressBar.visibility = View.GONE
                    binding.schoolsRecycler.visibility = View.VISIBLE
                    binding.swipeRefresh.visibility = View.VISIBLE

                    // set the fetched data to the adapter to populate the recyclerview
                    mAdapter.setSchoolsList(retrievedSongs)
                }
                is ResponseStatus.ERROR -> {
                    binding.progressBar.visibility = View.GONE
                    binding.schoolsRecycler.visibility = View.GONE
                    binding.swipeRefresh.visibility = View.GONE

                    displayErrors(state.error.localizedMessage) {
                        schoolsViewModel.getAllSchools()
                    }
                }
            }
        }

        schoolsViewModel.getAllSchools()

        // Inflate the layout for this fragment
        return binding.root
    }

    override fun onResume() {
        super.onResume()

        binding.swipeRefresh.apply {
            setColorSchemeResources(
                android.R.color.holo_blue_dark,
                android.R.color.holo_purple,
                android.R.color.holo_orange_dark,
                android.R.color.holo_blue_bright,
                android.R.color.holo_red_light,
                android.R.color.holo_green_dark,
            )
            setOnRefreshListener {
                schoolsViewModel.getAllSchools()
                binding.swipeRefresh.isRefreshing = false
            }
        }
    }

    /**
     * Navigate to the details screen to view selected School's SAT scores and more details obout it
     */
    override fun onItemClicked(school: School) {
        // set info of selected data to show on details screen
        schoolsViewModel.school = school
        // reset the score livedata to retrieve the updated score based on the school user selects
        schoolsViewModel.resetScore()
        findNavController().navigate(R.id.action_schoolsList_to_schoolDetails)
    }
}