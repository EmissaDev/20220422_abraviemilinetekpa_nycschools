package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.data

import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.School
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.Scores
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


interface DatabaseRepository {
    fun getAllSchools() : Flow<List<School>>
    fun getSchool(dbn: String) : Flow<School>
    fun getSchoolScore(dbn: String): Flow<Scores>
    suspend fun insertSchools(schools: List<School>)
    suspend fun insertSchool(school: School)
    suspend fun deleteSchool(school: School)
    suspend fun deleteSchools(schools: List<School>)
}

class DatabaseRepositoryImpl @Inject constructor(
    private val dao: SchoolsDao
): DatabaseRepository {

    override fun getAllSchools(): Flow<List<School>> {
        return dao.getAllSchools()
    }

    override fun getSchool(dbn: String): Flow<School> {
        return dao.getSchool(dbn)
    }

    override fun getSchoolScore(dbn: String): Flow<Scores> {
        return dao.getSchoolScore(dbn)
    }

    override suspend fun insertSchools(schools: List<School>) {
        return dao.insertSchools(schools)
    }

    override suspend fun insertSchool(school: School) {
        return dao.insertSchool(school)
    }

    override suspend fun deleteSchool(school: School) {
        return dao.deleteSchool(school)
    }

    override suspend fun deleteSchools(schools: List<School>) {
        return dao.deleteSchools(schools)
    }

}