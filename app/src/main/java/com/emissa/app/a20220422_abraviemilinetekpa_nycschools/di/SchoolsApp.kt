package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.di

import android.app.Application
import dagger.hilt.android.HiltAndroidApp


@HiltAndroidApp
class SchoolsApp : Application()
