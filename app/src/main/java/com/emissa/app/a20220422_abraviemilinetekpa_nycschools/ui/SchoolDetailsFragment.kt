package com.emissa.app.a20220422_abraviemilinetekpa_nycschools.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.databinding.FragmentSchoolDetailsBinding
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.model.Scores
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.BaseFragment
import com.emissa.app.a20220422_abraviemilinetekpa_nycschools.utils.ResponseStatus


/**
 * A simple [Fragment] subclass.
 * this view show when user click on the 'Read more' button on a item of the school list
 */
class SchoolDetailsFragment : BaseFragment() {
    private val binding by lazy {
        FragmentSchoolDetailsBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        val schoolInfo = schoolsViewModel.school

        // Observe the SAT scores to populate the view based on the network response
        // And the update scores based on user's selection
        // If time allowed, will populate local data when device is offline
        schoolsViewModel.satScore.observe(viewLifecycleOwner) { state ->
            when (state) {
                is ResponseStatus.LOADING -> {
                    showToastMessage("Loading More Info")
                }
                is ResponseStatus.SUCCESS<*> -> {
                    val scoresResponse = state.response as List<Scores>

                    /**
                     * There is an edge case while retrieving the ST scores from the endpoint
                     * The endpoint shows combined 'dbn' value in this form ["dbn": "21K412/21K411"]
                     *
                     * When retrieving SAT score, when the ["dbn"] of the school does not much
                     * Any ["dbn"] we receive an empty response from the scores endpoint
                     * Which lead to the app crashing. To solve the issue, we have the below check
                     */
                    val score: Scores? = scoresResponse.firstOrNull()
                    if (scoresResponse.isEmpty()) {
                        displayErrors("Sorry, no SAT score available!") {

                        }
                        binding.apply {
                            if (schoolInfo != null) {
                                schoolName.text = schoolInfo.schoolName
                                tvAddress.text = "Address: ${schoolInfo.location}"
                                tvEmail.text = "Email: ${schoolInfo.schoolEmail}"
                                tvWebsite.text = "Website: ${schoolInfo.website}"
                                tvOverview.text = schoolInfo.overview_paragraph
                            }
                        }
                    } else {
                        binding.apply {
                            if (schoolInfo != null) {
                                schoolName.text = schoolInfo.schoolName

                                scoreInfo.visibility = View.VISIBLE

                                tvSatScores.text =
                                    "SAT Test takers: ${score?.numOfSatTestTakers}"
                                tvMathScores.text = "Math: ${score?.satMathAvgScore}"
                                tvReadingScores.text =
                                    "Reading: ${score?.satCriticalReadingAvgScore}"
                                tvWritingScores.text =
                                    "Writing: ${score?.satWritingAvgScore}"

                                tvAddress.text = "Address: ${schoolInfo.location}"
                                tvEmail.text = "Email: ${schoolInfo.schoolEmail}"
                                tvWebsite.text = "Website: ${schoolInfo.website}"
                                tvOverview.text = schoolInfo.overview_paragraph
                            }
                        }
                    }
                }
                is ResponseStatus.ERROR -> {
                    displayErrors(state.error.localizedMessage) {}
                }
            }
        }
        if (schoolInfo != null) {
            schoolsViewModel.getSchoolScores(schoolInfo.dbn)
        }
        // Inflate the layout for this fragment
        return binding.root
    }
}